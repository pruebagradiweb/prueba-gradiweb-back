<?php

namespace App\Http\Controllers;

use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicles = Vehicle::all();
        return $vehicles;
    }

    public function vehiclesForBrand($brand_id)
    {
        // $vehicles = Vehicle::where('brand_id', $brand_id)->get();
        $vehicles = DB::table('vehicles')
        ->where('brand_id', $brand_id)
            ->join('users', 'users.id', '=', 'vehicles.user_id')
            ->select('vehicles.*', 'users.name as propietario')
            ->get();
        return $vehicles;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vehicle = new Vehicle;
        $vehicle->type = $request->type;
        $vehicle->license_plate = $request->license_plate;
        $vehicle->brand_id = $request->brand_id;
        $vehicle->user_id = $request->user_id;
        $vehicle->save();
        return response()->json([
            'result' => 'Vehículo creado satisfactoriamente',
            'state' => '200',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicle $vehicle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vehicle = Vehicle::find($id);
        $vehicle->delete();
        return response("Vehículo eliminado satisfactoriamente", 200);
    }
}
