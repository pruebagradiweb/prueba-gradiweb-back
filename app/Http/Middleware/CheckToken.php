<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;

//use mysql_xdevapi\Exception;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    // Función que define el middleware, si el usuario está logueado, dejará pasar las peticiones
    public function handle(Request $request, Closure $next){
        $headerss = $request->headers->all();
        if($headerss['authorization'][0]){
            $authorization = $headerss['authorization'][0];
        }
        if($authorization == env('CheckToken')){
            return $next($request);
        }else{
            dd("Acceso no permitido");
        }
    }
}
