<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VehicleTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testVehiclesGetRequest()
    {
        $token = env('CheckToken');
          $response = $this->withHeader('authorization', $token)
        ->json('get', '/api/vehicles');

        $response->assertStatus(200);
    }

    public function testVehiclesForBrandRequest()
    {
        $token = env('CheckToken');
          $response = $this->withHeader('authorization', $token)
        ->json('get', '/api/vehicles-for-brand/1');

        $response->assertStatus(200);
    }
}
