<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testBrandsGetRequest()
    {
        $token = env('CheckToken');
          $response = $this->withHeader('authorization', $token)
        ->json('get', '/api/brands');

        $response->assertStatus(200);
    }
}
