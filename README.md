<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Instalación
 - clonar el proyecto con la siguiente línea de comando: git clone https://gitlab.com/pruebagradiweb/prueba-gradiweb-back.git
 - crear archivo .env copiando archivo .env.example, editar la configuración de base de datos y agregar la variable del token para la API.
 - instalar dependencias con el siguiente comando: composer install
 - ejecutar el siguiente comando para crear la llave: php artisan key:generate

# Explicación del proyecto (api)

- Existen tres modelos: User, Brand y Vehicle

- Estos modelos se relacionan así, un usuario puede tener 0 o varios vehículos, un vehículo debe tener una marca y pertenecer a un usuario. Por lo tanto si no existen al menos una marca un usuario en la base de datos, se generará un error y no se podrán registrar vehículos

- Al ejecutar las pruebas, se probará la indexación de vehículos, marcas y usuarios (ejecutar las pruebas con "php artisan test"

# Los endpoints son los siguientes:

Nota: Debe enviarse el token de autorización en los headers de cada petición de la siguiente manera:
authorization : CJhbGciOiJSUzI1NiIsImp0aSI6IjMyZjY1YzA3M2IzV1QiLNTdiNmMwNjJhMzQ0M2EMGViMjhiZ

- get: https://"url"/api/brands: Retornará todas las marcas

- post: https://"url"/api/brands: Se usará para crear las marcas, deben pasarse los siguientes parámetros en el body:

{
    "name":"string",
    "description":"string"
}

- delete: https://"url"/api/brands/{marca-id}: Eliminará la marca deseada


- get: https://"url"/api/vehicles: Retornará todos los vehículos

- post: https://"url"/api/vehicles: Se utilizará para crear los vehículos, debe pasarse el body con la siguiente estructura
{
    "type":"String",
    "license_plate":"String de mínimo 6 caracteres y máximo 8",
    "brand_id":"id de la marca",
    "user_id":"id del usuario"
}

- delete: https://"url"/api/vehicles/{vehiculo-id}: Eliminará el vehículo deseado

- get: https://"url"/api/users: Listará todos los usuarios

- post: https://"url"/api/users: Creará un nuevo usuario, debe pasarse el body con la siguiente estructura:
{
    "name":"nombre del usuario",
    "email":"correo del usuario",
    "password":"contraseña",
    "confirm_password":"confirmación de la contraseña"
}

- delete: https://"url"/api/users/{usuario-id}: Eliminará el usuario deseado

